#!/usr/bin/env bash
#https://sourceforge.net/projects/geoserver/files/GeoServer/2.8.2/extensions/
# exit if anything returns failure
set -e
cd /etc/yum.repos.d/
wget  https://yum.boundlessps.com/geoshape.repo
wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm
#wget -O mbtiles.zip http://ares.opengeo.org/geoserver/master/community-latest/geoserver-2.11-SNAPSHOT-mbtiles-plugin.zip
#wget -O geoserver-master.zip http://ares.opengeo.org/geoserver/master/geoserver-master-latest-war.zip
rpm -Uvh epel-release-latest-6.noarch.rpm
rpm -Uvh http://yum.postgresql.org/9.6/redhat/rhel-6-x86_64/pgdg-centos96-9.6-3.noarch.rpm
yum -y install geoshape geoshape-geoserver elasticsearch postgis-postgresql95 rabbitmq-server-3.6.1
yum -y install http://opensource.wandisco.com/centos/6/git/x86_64/wandisco-git-release-6-1.noarch.rpm
yum -y install git
yum update -y curl libcurl
#sudo geoshape-config init 192.168.10.221
#sudo geoshape-config init localhost
#Required path
#geonode home - /var/lib/geonode/geonode
#rogue_geonode - /var/lib/geonode/rogue_geonode
#mkdir -p /var/lib/geonode/rogue_geonode/geoshape
#mkdir -p /var/lib/geonode/lib/python2.7/site-packages/
#https://accountname@bitbucket.org/<accountname>/<reponame>
#Create a new user for this.
sudo git clone https://vtadronesbuilder:19yfzGqK!k8D@bitbucket.org/vtadrones_geoshape/geoshape-vagrant.git
#sudo git clone https://vtadronesbuilder:19yfzGqK!k8D@bitbucket.org/vtadrones_geoshape/geonode.git
sudo git clone https://github.com/GeoNode/geonode.git
sudo git clone https://vtadronesbuilder:19yfzGqK!k8D@bitbucket.org/vtadrones_geoshape/geotools.git
sudo git clone https://vtadronesbuilder:19yfzGqK!k8D@bitbucket.org/vtadrones_geoshape/maploom.git
sudo git clone https://vtadronesbuilder:19yfzGqK!k8D@bitbucket.org/vtadrones_geoshape/rogue_geonode.git
sudo git clone https://vtadronesbuilder:19yfzGqK!k8D@bitbucket.org/vtadrones_geoshape/geoserver.git

#boundless version
#sudo git clone https://github.com/boundlessgeo/geonode.git
#sudo git clone https://github.com/boundlessgeo/geotools.git
#sudo git clone https://github.com/boundlessgeo/MapLoom.git
#sudo git clone https://github.com/boundlessgeo/rogue_geonode.git


sudo yum -y install unzip
#copy the server image over top of the install
sudo cp -r /etc/yum.repos.d/geoshape-vagrant/lib/ /var/
#copy the geonode repo over the install
sudo cp -r /etc/yum.repos.d/geonode/ /var/lib/
#copy geoshape
sudo cp -r /etc/yum.repos.d/rogue_geonode/ /var/lib/geonode/
#copy maploom
sudo yum -y install nodejs npm --enablerepo=epel


#copy geotools
#/etc/yum.repos.d/geotools 
#sudo service tomcat8 stop

echo "tomcat stopped"
#cd geoserver/src/Production/
#sudo rm -rf /var/lib/tomcat8/webapps/geoserver
#echo "Removed old files"
#ls -la /var/lib/tomcat8/webapps/
#sudo unzip -o geoserver-master.zip /var/lib/tomcat8/webapps/
#sudo unzip -o mbtiles.zip /var/lib/tomcat8/webapps/geoserver/WEB-INF/lib/
#sudo service postgresql-9.5 initdb
#sudo service elasticsearch start
#sudo service htcacheclean start
#sudo service httpd start
#sudo service geoshape start
#sudo service tomcat8 start
#echo "Install complete"
#service tomcat8 status
#sudo service lvmetad start
#sudo service dmeventd start
#sudo service mdmonitor start
#sudo service rpc.svcgssd start
#sudo service rpc.mountd start
#sudo service nfsd start
#sudo service rdisc start
#sudo service restorecond start
#sudo service rpc.gssd start
#sudo service rpc.idmapd start
#sudo service rpc.svcgssd start
#sandbox is stopped
#saslauthd is stopped
#echo "Started on http://192.168.10.221:8080/geoserver/web"
sudo service --status-all

#echo "To complete the install execute the following two lines"
#echo "vagrant ssh"
#echo "sudo geoshape-config init 192.168.10.221"
#echo "Verify Geonode URL"
cd /etc/yum.repos.d/maploom  
sudo npm -g install grunt-cli karma bower
sudo npm install
bower install --allow-root
#grunt watch
echo "sudo geoshape-config"
echo "Usage:"
        echo "geoshape-config database"
        echo "geoshape-config selinux"
        echo "geoshape-config updateip <public ip or domain>"
        echo "geoshape-config updatelayers"
        echo "geoshape-config rebuild_index"
        echo "geoshape-config syncdb"
        echo "geoshape-config collectstatic"
        echo "geoshape-config changepassword <username>"
        echo "geoshape-config createsuperuser"
        echo "geoshape-config shell"
        echo "geoshape-config dbshell"
        echo "geoshape-config start_all"
            echo "restarts all GeoSHAPE services"
        echo "geoshape-config stop_all"
            echo "stops all GeoSHAPE services"
        echo "geoshape-config init <ip or domain>"
            echo "chains together ssl, database, django, updateip,"
            echo "collectstatic, syncdb, selinux, start_all,"
            echo "updatelayers, rebuildindex and createsuperuser"
        echo "geoshape-config maploom_dev"
            echo "modify geonode to point to the a build of maploom"
            echo "on the host machine to speedup the edit-build-test"
            echo "cycle of maploom"
        echo "geoshape-config ssl <public ip or domain>"
echo "IMPORTANT: Run 'sudo geoshape-config init 192.168.10.221' before logging into the GeoShape - Username and password for geoserver is admin/geoserver"
